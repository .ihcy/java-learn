package org.example.spring5;

import reactor.core.publisher.Mono;

/**
 * Mono 实现了 org.reactivestreams.Publisher 接口，代表0到1个元素的发布者（Publisher）
 */
public class MonoDemo {

    public static void main(String[] args) {
        // 创建一个新的Mono来发出指定的项目，该项目在实例化时被捕获。
        Mono<String> mono1 = Mono.just("test Mono");
        // 创建一个Mono ，一旦提供的Runnable被执行，它就会完成为空。
        Mono.fromRunnable(() -> {
            System.out.println("test Mono");
        });
    }
}
