package org.example.spring5;

import reactor.core.publisher.Flux;

/**
 * Flux 同样实现了 org.reactivestreams.Publisher 接口，代表0到N个元素的发布者（Publisher）
 */
public class FluxDemo {

    public static void main(String[] args) {
        Flux flux = Flux.empty();
    }
}
