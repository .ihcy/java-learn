package org.example.spring.ioc.source.config;

/**
 * @ClassName PropertyValue
 * @Description PropertyValue就封装着一个property标签的信息
 * @Author ihcy
 * @Date 2019/7/23 15:55
 * @Version 1.0
 **/
public class PropertyValue {

    private String name;

    private Object value;

    public PropertyValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
