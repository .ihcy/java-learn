package org.example.spring.ioc.source.config;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName BeanDefinition
 * @Description Bean 定义信息
 * @Author ihcy
 * @Date 2019/7/23 14:10
 * @Version 1.0
 **/
public class BeanDefinition {
    private String beanName;
    private String beanClassName;
    private String initMethod;

    public BeanDefinition() {
    }

    public BeanDefinition(String beanName, String beanClassName) {
        this.beanName = beanName;
        this.beanClassName = beanClassName;
    }

    /**
     * bean中的属性信息
     */
    private List<PropertyValue> propertyValues = new ArrayList<>();

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanClassName() {
        return beanClassName;
    }

    public void setBeanClassName(String beanClassName) {
        this.beanClassName = beanClassName;
    }

    public String getInitMethod() {
        return initMethod;
    }

    public void setInitMethod(String initMethod) {
        this.initMethod = initMethod;
    }

    public List<PropertyValue> getPropertyValues() {
        return propertyValues;
    }

    public void addPropertyValue(PropertyValue propertyValue) {
        propertyValues.add(propertyValue);
    }
}
