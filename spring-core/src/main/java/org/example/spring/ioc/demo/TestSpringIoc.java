package org.example.spring.ioc.demo;

import org.example.spring.ioc.source.factory.BeanFactory;
import org.example.spring.ioc.source.factory.DefaultListableBeanFactory;
import org.junit.Test;

/**
 * @ClassName TestSpringIoc
 * @Description
 * @Author ihcy
 * @Date 2019/7/23 13:57
 * @Version 1.0
 **/
public class TestSpringIoc {

    @Test
    public void test(){
        // 指定资源路径
        String location = "classpath:beans.xml";
        // 创建工厂
        BeanFactory beanFactory = new DefaultListableBeanFactory(location);
        // 获取对象
        Student student = (Student) beanFactory.getBean("student");
        System.out.println(student);
    }
}
