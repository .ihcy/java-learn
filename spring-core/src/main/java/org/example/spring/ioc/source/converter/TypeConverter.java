package org.example.spring.ioc.source.converter;

public interface TypeConverter {

	boolean isType(Class<?> clazz);
	
	Object convert(String source);
}
