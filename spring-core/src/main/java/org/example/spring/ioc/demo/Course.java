package org.example.spring.ioc.demo;

/**
 * @ClassName Course
 * @Description 课程
 * @Author ihcy
 * @Date 2019/7/23 13:57
 * @Version 1.0
 **/
public class Course {
    private String name;

    private Integer age;

    private String teacherName;

    public Course() {
    }

    public Course(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Course{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
