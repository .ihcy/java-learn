package org.example.spring.ioc.source.config;

import java.io.InputStream;

/**
 * @ClassName ClassPathResource
 * @Description
 * @Author ihcy
 * @Date 2019/7/23 14:39
 * @Version 1.0
 **/
public class ClassPathResource implements Resource {

    private String location;

    @Override
    public boolean isCanRead(String location) {
        if (location == null || "".equals(location)) {
            return false;
        }
        if(location.startsWith("classpath:")){
            this.location = location.replace("classpath:","");
            return true;
        }
        return false;
    }

    @Override
    public InputStream getInputStream() {
        if (location == null || "".equals(location)) {
            return null;
        }
        return this.getClass().getClassLoader().getResourceAsStream(location);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
