package org.example.spring.ioc.source.config;

import java.io.InputStream;

/**
 * @ClassName Resource
 * @Description Spring 资源路径类
 * @Author ihcy
 * @Date 2019/7/23 14:37
 * @Version 1.0
 **/
public interface Resource {

    /**
     * 路径是否可以被加载
     * @param location
     * @return
     */
    boolean isCanRead(String location);

    /**
     * 获取流对象
     * @return
     */
    InputStream getInputStream();
}
