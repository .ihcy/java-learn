package org.example.spring.ioc.source.config;

import org.example.spring.ioc.source.factory.DefaultListableBeanFactory;
import org.example.spring.ioc.source.util.DocumentReader;
import org.dom4j.Document;

/**
 * @ClassName XmlBeanDefinitionReader
 * @Description
 * @Author ihcy
 * @Date 2019/7/23 14:16
 * @Version 1.0
 **/
public class XmlBeanDefinitionReader {

    /**
     * 加载配置文件信息，并封装BeanDefinitions
     * @param beanFactory
     * @param resource
     */
    public void loadBeanDefinitions(DefaultListableBeanFactory beanFactory, Resource resource) {
        // 读取配置文件信息
        Document document = DocumentReader.createDocument(resource.getInputStream());
        XmlBeanDefinitionDocumentReader xmlBeanDefinitionDocumentReader = new XmlBeanDefinitionDocumentReader(beanFactory);
        xmlBeanDefinitionDocumentReader.loadBeanDefinitions(document);
    }
}
