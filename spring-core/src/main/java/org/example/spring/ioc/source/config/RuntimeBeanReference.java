package org.example.spring.ioc.source.config;

/**
 * @ClassName RuntimeBeanReference
 * @Description
 * @Author ihcy
 * @Date 2019/7/23 16:06
 * @Version 1.0
 **/
public class RuntimeBeanReference {
    private String ref;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public RuntimeBeanReference(String ref) {
        this.ref = ref;
    }

}
