package org.example.spring.ioc.source.factory;

/**
 * @ClassName AbstractBeanFactory
 * @Description spring bean 抽象实现类[对BeanFactory 的扩展]
 * @Author ihcy
 * @Date 2019/7/23 14:01
 * @Version 1.0
 **/
public abstract class AbstractBeanFactory implements BeanFactory {
    @Override
    public Object getBean(String name) {
        return null;
    }

    @Override
    public <T> T getBean(Class<T> clazz) {
        return null;
    }
}
