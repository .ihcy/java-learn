package org.example.spring.ioc.source.factory;

/**
 * @ClassName BeanFactory
 * @Description spring Bean 工厂
 * @Author ihcy
 * @Date 2019/7/23 13:55
 * @Version 1.0
 **/
public interface BeanFactory {

    /**
     * 根据名称获取对象
     * @param name
     * @return
     */
    Object getBean(String name);

    /**
     * 根据类型获取对象
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T getBean(Class<T> clazz);
}
