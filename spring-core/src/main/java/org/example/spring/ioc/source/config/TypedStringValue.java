package org.example.spring.ioc.source.config;

/**
 * @ClassName TypedStringValue
 * @Description
 * @Author ihcy
 * @Date 2019/7/23 16:03
 * @Version 1.0
 **/
public class TypedStringValue {

    private String value;

    private Class<?> targetType;

    public TypedStringValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Class<?> getTargetType() {
        return targetType;
    }

    public void setTargetType(Class<?> targetType) {
        this.targetType = targetType;
    }
}
