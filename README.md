# java-learn
> 个人关于 java 相关的学习以及验证

* 设计模式
* 数据结构
* 风控引擎设计
* JDK 相关基础
* 基本框架学习

## 设计模式 design patterns
[说明文档](design-patterns/README.md)

## 风控引擎 engine
[说明文档](engine/README.md)

## mybatis 模仿学习开发 mybatis-core
[说明文档](mybatis-core/README.md)

## spring ioc 模仿学习开发 spring-core
