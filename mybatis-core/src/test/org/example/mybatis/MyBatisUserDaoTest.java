package org.example.mybatis;

import org.example.mybatis.core.sqlsession.SqlSessionFactory;
import org.example.mybatis.core.sqlsession.SqlSessionFactoryBuilder;
import org.example.mybatis.demo.dao.MybatisUserDao;
import org.example.mybatis.demo.dao.impl.MybatisUserDaoImpl;
import org.example.mybatis.demo.po.MybatisUser;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

/**
 * @ClassName MyBatisUserDaoTest
 * @Description TODO
 * @Author ihcy
 * @Date 2019/7/6 18:46
 * @Version 1.0
 **/
public class MyBatisUserDaoTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init(){
        // 创建sqlSessionFactory
        String resource = "SqlMapConfig.xml";
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

    }
    @Test
    public void selectByIdTest(){
        Long id  = 1L;
        MybatisUserDao userDao = new MybatisUserDaoImpl(sqlSessionFactory);
        MybatisUser user = userDao.selectById(id);
        System.out.println(user);
    }
}
