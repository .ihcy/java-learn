package org.example.mybatis.demo.dao;


import org.example.mybatis.demo.po.MybatisUser;

/**
 * @InterfaceName MybatisUserDao
 * @Version 1.0
 **/
public interface MybatisUserDao {

    MybatisUser selectById(Long id);



}
