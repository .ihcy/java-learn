package org.example.mybatis.demo.po;

/**
 * @ClassName MybatisUser
 * @Version 1.0
 *
 * CREATE TABLE `mybatis_user` (
 *   `id` bigint(20) NOT NULL,
 *   `name` varchar(50) DEFAULT NULL,
 *   `age` int(11) DEFAULT NULL,
 *   `desc` varchar(255) DEFAULT NULL,
 *   PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
 **/
public class MybatisUser {

    private Long id;
    private String name;
    private Integer age;
    private String desc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "MybatisUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", desc='" + desc + '\'' +
                '}';
    }
}
