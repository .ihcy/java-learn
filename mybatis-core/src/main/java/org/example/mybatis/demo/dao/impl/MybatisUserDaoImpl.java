package org.example.mybatis.demo.dao.impl;


import org.example.mybatis.core.sqlsession.SqlSession;
import org.example.mybatis.core.sqlsession.SqlSessionFactory;
import org.example.mybatis.demo.dao.MybatisUserDao;
import org.example.mybatis.demo.po.MybatisUser;

/**
 * @ClassName MybatisUserDaoImpl
 * @Version 1.0
 **/
public class MybatisUserDaoImpl implements MybatisUserDao {



    private SqlSessionFactory sqlSessionFactory;

    public MybatisUserDaoImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public MybatisUser selectById(Long id) {
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();
        String statementId = "test.findUserById";
        MybatisUser user = sqlSession.selectOne(statementId,id);
        return user;
    }
}
