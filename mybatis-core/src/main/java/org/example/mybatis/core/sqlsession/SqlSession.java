package org.example.mybatis.core.sqlsession;

import java.util.List;

/**
 * @InterfaceName SqlSession
 * @Version 1.0
 **/
public interface SqlSession {

    /**
     *
     * @param statementId 参数位置
     * @param param sql参数
     * @param <T> 返回类型
     * @return
     */
    <T> T selectOne(String statementId, Object param);

    <T> List<T> selectList(String statementId, Object param);
}
