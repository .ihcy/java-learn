package org.example.mybatis.core.config;

import org.example.mybatis.core.util.GenericTokenParser;
import org.example.mybatis.core.util.ParameterMappingTokenHandler;

/**
 * @ClassName SqlSource
 * @Description 封装没有解析的sql语句
 * @Version 1.0
 **/
public class SqlSource {

    /**
     * select * from user where id = #{id}
     */
    private String sqlText;

    public SqlSource(String sqlText) {
        this.sqlText = sqlText;
    }

    /**
     * 解析sql文本
     * @return
     */
    public BoundSql getBoundSql(){
        // 解析sql文本
        ParameterMappingTokenHandler tokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", tokenHandler);
        String sql = genericTokenParser.parse(sqlText);
        // 就是将解析之后的SQL语句，和解析出来的SQL参数使用组合模式绑定到一个类中
        System.out.println("/***** 解析sql文本 start *********/");
        System.out.println(sql);
        System.out.println(tokenHandler.getParameterMappings());
        System.out.println("/***** 解析sql文本 end *********/");
        return new BoundSql(sql,tokenHandler.getParameterMappings());
    }


    @Override
    public String toString() {
        return "SqlSource{" +
                "sqlText='" + sqlText + '\'' +
                '}';
    }
}
