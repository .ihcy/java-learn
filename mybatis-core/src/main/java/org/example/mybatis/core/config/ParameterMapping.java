package org.example.mybatis.core.config;

/**
 * @ClassName ParameterMapping
 * @Description 参数名称
 * @Version 1.0
 **/
public class ParameterMapping {
    private String name;

    public ParameterMapping(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ParameterMapping{" +
                "name='" + name + '\'' +
                '}';
    }
}
