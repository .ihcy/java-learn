package org.example.mybatis.core.sqlsession;


import org.example.mybatis.core.config.Configuration;

/**
 * @ClassName DefaultSqlSessionFactory
 * @Version 1.0
 **/
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSqlSession() {
        return new DefaultSqlSession(configuration);
    }
}
