package org.example.mybatis.core.sqlsession;



import org.example.mybatis.core.config.Configuration;
import org.example.mybatis.core.config.MappedStatement;

import java.util.List;



public interface Executor {

	<T> List<T> query(Configuration configuration, MappedStatement mappedStatement, Object param);
}
