package org.example.mybatis.core.sqlsession;

import org.dom4j.Document;
import org.example.mybatis.core.config.Configuration;
import org.example.mybatis.core.config.XMLConfigParser;

import java.io.InputStream;
import java.io.Reader;

/**
 * @ClassName SqlSessionFactoryBuilder
 * @Version 1.0
 **/
public class SqlSessionFactoryBuilder {

    private Configuration configuration;

    public SqlSessionFactoryBuilder() {
        configuration = new Configuration();
    }

    /**
     * 解析流信息； 封装 configuration,构建 SqlSessionFactory
     */
    public SqlSessionFactory build(InputStream inputStream) {
        Document document = DocumentReader.createDocument(inputStream);
        XMLConfigParser xmlConfigParser = new XMLConfigParser(configuration);
        configuration = xmlConfigParser.parseConfiguration(document.getRootElement());
        return build();
    }

    public SqlSessionFactory build(Reader reader) {
        return build();
    }

    private SqlSessionFactory build() {
        return new DefaultSqlSessionFactory(configuration);
    }
}
