package org.example.mybatis.core.config;

import java.util.List;

/**
 * @ClassName BoundSql
 * @Description 解析之后的sql语句和解析出来的sql参数使用组合模式绑定到一个类中
 * @Version 1.0
 **/
public class BoundSql {

    /**
     * 解析后的sql
     */
    private String sql;

    /**
     * 解析出来的参数
     */
    private List<ParameterMapping> parameterMappings ;

    public BoundSql(String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void addParameterMapping(ParameterMapping parameterMapping) {
        this.parameterMappings.add(parameterMapping);
    }

    @Override
    public String toString() {
        return "BoundSql{" +
                "sql='" + sql + '\'' +
                ", parameterMappings=" + parameterMappings +
                '}';
    }
}
