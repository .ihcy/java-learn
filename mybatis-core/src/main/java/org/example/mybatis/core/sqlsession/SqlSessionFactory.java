package org.example.mybatis.core.sqlsession;

/**
 * @InterfaceName SqlSessionFactory
 * @Version 1.0
 **/
public interface SqlSessionFactory {

    SqlSession openSqlSession();

}
