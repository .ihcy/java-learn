package org.example.mybatis.jdbc;

import org.junit.Test;

import java.sql.*;

/**
 * @ClassName JdbcTest
 * @Version 1.0
 **/
public class JdbcTest {

    @Test
    public void test(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/eight-thousand?" +
                            "useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false",
                    "root",
                    "root");

            String sql = "select * from mybatis_user where id = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,1L);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                System.out.println(resultSet.getString("id"));
                System.out.println(resultSet.getString("name"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(resultSet!=null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(preparedStatement!=null){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
