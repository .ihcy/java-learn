# 模仿 mybatis 的框架简易编写

## 核心类
* SqlSession：  sql 的方法映射
* SqlSessionFactory： sqlSession工类
* SqlSessionFactoryBuilder : SqlSession工厂构建者
* Executor ： 执行器，SQL 的最终执行器
* BoundSql : 绑定解析出来的sql 和 sql 参数
* SqlSource : 封装没有解析的sql语句