package org.example.es;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

public abstract class ElasticSearchTask {
    public String userIndex = "user";

    void execute() throws IOException {
        RestHighLevelClient client = new RestHighLevelClient
                (RestClient.builder(new HttpHost("localhost", 9200, "http")));
        doSomething(client);
        client.close();
    }

    abstract void doSomething(RestHighLevelClient client) throws IOException;
}
