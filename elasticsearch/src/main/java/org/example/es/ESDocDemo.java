package org.example.es;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.example.utils.UserInfoGeneratorUtil;

import java.io.IOException;
import java.util.Arrays;

public class ESDocDemo {
    public static void main(String[] args) throws IOException {
//        addDoc();
//        updateDoc();
//        getDoc();
//        deleteDoc();
//        batchDeleteDoc();
        batchAddDoc();
    }

    /**
     * 添加文档
     */
    public static void addDoc() throws IOException {
        new ElasticSearchTask() {
            @Override
            void doSomething(RestHighLevelClient client) throws IOException {
                IndexRequest request = new IndexRequest();
                request.index(userIndex).id("1001");
                User user = new User("张三", 18, "男");
                // user -> json
                ObjectMapper objectMapper = new ObjectMapper();
                String userJson = objectMapper.writeValueAsString(user);
                // 添加文档为Json格式
                request.source(userJson, XContentType.JSON);
                IndexResponse response = client.index(request, RequestOptions.DEFAULT);
                // 打印结果
                System.out.println("_index: " + response.getIndex());
                System.out.println("_id: " + response.getId());
                System.out.println("_result:" + response.getResult());
            }
        }.execute();
    }

    /**
     * 编辑文档
     */
    public static void updateDoc() throws IOException {
        new ElasticSearchTask() {
            @Override
            void doSomething(RestHighLevelClient client) throws IOException {
                UpdateRequest request = new UpdateRequest();
                request.index(userIndex).id("1001");
                request.doc(XContentType.JSON, "sex", "女");
                UpdateResponse response = client.update(request, RequestOptions.DEFAULT);
                // 打印结果
                System.out.println("_index: " + response.getIndex());
                System.out.println("_id: " + response.getId());
                System.out.println("_result:" + response.getResult());
            }
        }.execute();
    }

    /**
     * 获取文档
     */
    public static void getDoc() throws IOException {
        new ElasticSearchTask() {
            @Override
            void doSomething(RestHighLevelClient client) throws IOException {
                GetRequest request = new GetRequest().index(userIndex).id("1001");
                GetResponse response = client.get(request, RequestOptions.DEFAULT);
                System.out.println("_index: " + response.getIndex());
                System.out.println("_id: " + response.getId());
                System.out.println("_type:" + response.getType());
                System.out.println("source:" + response.getSourceAsString());
            }
        }.execute();
    }

    /**
     * 批量添加
     */
    public static void batchAddDoc() throws IOException {
        new ElasticSearchTask() {
            @Override
            void doSomething(RestHighLevelClient client) throws IOException {
                ObjectMapper objectMapper = new ObjectMapper();
                // 再次执行添加会覆盖之前的数据
                BulkRequest request = new BulkRequest();
                for (int i = 0; i < 50; i++) {
                    User user1 = new User(UserInfoGeneratorUtil.getRandomName(), i, "男");
                    request.add(new IndexRequest()
                            .index(userIndex)
                            .id(String.valueOf(i + 400))
//                            .source(XContentType.JSON, "name", UserInfoGeneratorUtil.getRandomName(), "age", i)
                            .source(objectMapper.writeValueAsString(user1), XContentType.JSON));

                    User user = new User(UserInfoGeneratorUtil.getRandomName(), i, "女");
                    request.add(new IndexRequest()
                            .index(userIndex)
                            .id(String.valueOf(i + 700))
//                            .source(XContentType.JSON, "name", UserInfoGeneratorUtil.getRandomName(), "age", i)
                            .source(objectMapper.writeValueAsString(user), XContentType.JSON));
                }
                BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
                System.out.println("_took:" + response.getTook());
                System.out.println("_items:" + Arrays.toString(response.getItems()));
            }
        }.execute();
    }

    /**
     * 批量删除
     */
    public static void batchDeleteDoc() throws IOException {
        new ElasticSearchTask() {
            @Override
            void doSomething(RestHighLevelClient client) throws IOException {
                // ID 不存在，不会报错。
                BulkRequest request = new BulkRequest();
                for (int i = 0; i < 3; i++) {
                    request.add(new DeleteRequest().index("user").id(String.valueOf(i + 1000)));
                }
                BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
                System.out.println("_took:" + response.getTook());
                System.out.println("_items:" + response.getItems());
            }
        }.execute();
    }

    /**
     * 删除
     */
    public static void deleteDoc() throws IOException {
        new ElasticSearchTask() {
            @Override
            void doSomething(RestHighLevelClient client) throws IOException {
                DeleteRequest request = new DeleteRequest().index(userIndex).id("1001");
                DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
                System.out.println(response.toString());
            }
        }.execute();
    }
}
