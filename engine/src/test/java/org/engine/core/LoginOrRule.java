package org.engine.core;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 逻辑或规则，可作为root规则
 * 此 Rule 本身并不含有逻辑，专注于 childrenRule 的执行结果。
 */
@Component
@Scope("prototype")
public class LoginOrRule extends AbstractRule {
    @Override
    protected ExecuteResult executeBody(RuleEngineContext ruleEngineContext) {
        ExecuteResult result = ExecuteResult.successResult();
        for (int i = 0; i < this.childrenRule.size(); i++) {
            AbstractRule childrenRule = this.childrenRule.get(i);
            ExecuteResult childExecuteResult = childrenRule.execute(ruleEngineContext);
            // 非静默执行，则参与最后结果判断
            if (!childrenRule.ignoreResult) {
                result.addChildResult(childExecuteResult);
                result.setSuccessful(childExecuteResult.isSuccessful() || result.isSuccessful());
            }
            // 非子元素全部执行，遇到错误结果即返回结果
            if (!this.executeAllChildren && !result.isSuccessful()) {
                return result;
            }
        }
        return result;
    }

}
