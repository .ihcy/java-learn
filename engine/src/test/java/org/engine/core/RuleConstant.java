package org.engine.core;

public class RuleConstant {
    public static final String PARSE_ID = "id";
    public static final String PARSE_CLASS = "class";
    public static final String PARSE_NAME = "name";
    public static final String PARSE_SILENCE = "silence";
    public static final String PARSE_EXECUTE_ALL = "executeAll";
    public static final String PARSE_RULE_CONFIG = "ruleConfig";
    public static final String PARSE_CHILDREN = "children";
}
