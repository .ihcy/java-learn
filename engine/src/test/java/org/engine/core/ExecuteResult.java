package org.engine.core;

import java.util.List;

/**
 * 风控策略规则执行结果
 */
public class ExecuteResult {
    /**
     * 是否成功
     */
    private boolean isSuccessful;

    /**
     * 耗时
     */
    private long consumingTime;

    /**
     * 规则
     */
    private AbstractRule abstractRule;

    private String message;
    /**
     * 子规则结果
     */
    private List<ExecuteResult> childrenRuleResult;

    public ExecuteResult(boolean b) {
    }

    public ExecuteResult(boolean isSuccessful, String message) {
        this.isSuccessful = isSuccessful;
        this.message = message;
    }

    public static ExecuteResult failResult(String message) {
        return new ExecuteResult(false, message);
    }

    public static ExecuteResult successResult() {
        return new ExecuteResult(true);
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public long getConsumingTime() {
        return consumingTime;
    }

    public void setConsumingTime(long consumingTime) {
        this.consumingTime = consumingTime;
    }

    public AbstractRule getAbstractRule() {
        return abstractRule;
    }

    public void setAbstractRule(AbstractRule abstractRule) {
        this.abstractRule = abstractRule;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ExecuteResult> getChildrenRuleResult() {
        return childrenRuleResult;
    }

    public void setChildrenRuleResult(List<ExecuteResult> childrenRuleResult) {
        this.childrenRuleResult = childrenRuleResult;
    }

    public void addChildResult(ExecuteResult result) {
        this.childrenRuleResult.add(result);
    }
}
