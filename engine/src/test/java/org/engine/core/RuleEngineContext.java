package org.engine.core;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 风控引擎上下文，缓存数据
 */
public class RuleEngineContext {

    /**
     * 用户数据缓存
     */
    private Map<String, Object> dataCacheMap = new HashMap<>();
    /**
     * 规则结果缓存
     */
    private Map<String, ? extends AbstractRule> ruleResultCacheMap = new HashMap<>();

    /**
     * 请求或者其他参数
     */
    private JSONObject params;

    public RuleEngineContext(JSONObject params) {
        this.params = params;
    }

    public Map<String, Object> getDataCacheMap() {
        return dataCacheMap;
    }

    public void setDataCacheMap(Map<String, Object> dataCacheMap) {
        this.dataCacheMap = dataCacheMap;
    }

    public Map<String, ? extends AbstractRule> getRuleResultCacheMap() {
        return ruleResultCacheMap;
    }

    public void setRuleResultCacheMap(Map<String, ? extends AbstractRule> ruleResultCacheMap) {
        this.ruleResultCacheMap = ruleResultCacheMap;
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }
}
