package org.engine.service;

import com.alibaba.fastjson.JSONObject;
import org.engine.core.ExecuteResult;

public interface ILogWriter {
    /**
     * TODO 保存日志
     */
    void addLog(ExecuteResult executeResult, JSONObject param);
}
