package org.engine.service;

import com.alibaba.fastjson.JSONObject;
import org.engine.core.ExecuteResult;

public interface IRuleService {
    /**
     * 载入所有规则
     */
    void loanALlService();

    /**
     * 重载规则
     */
    void reLoanAllService();

    /**
     * 执行规则
     *
     * @param ruleId 规则id
     * @param params 规则参数
     * @return
     */
    ExecuteResult execute(String ruleId, JSONObject params);
}
