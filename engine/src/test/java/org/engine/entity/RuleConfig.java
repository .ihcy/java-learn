package org.engine.entity;

/**
 * 规则配置实体。 和数据库坐持久化对应
 */
public class RuleConfig {
    private String id;

    /**
     * JSON格式字符串，只要存储相对应的规则
     */
    private String detail;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
