package org.example.design.pattern.proxypattern;

public class RealSubject implements Subject {

    @Override
    public void doSomething(String str1, int i) {
        System.out.println (String.format ("RealSubject String: %s ,int : %d", str1, i));
    }

    @Override
    public String getString(String str) {
        return "RealSubject.getString():" + str;
    }
}
