package org.example.design.pattern.observerpattern.example.two;

public class Teacher implements BellEventListener{
    @Override
    public void heardBell(RingEvent event) {
        if(event.isSound()){
            System.out.println("老师，上课了");
        }else {
            System.out.println("老师，下课了");
        }
    }
}
