package org.example.design.pattern.chapterI;

import java.util.HashMap;
import java.util.TreeMap;

public class Client {
    public static void main(String[] args) {
        Father f = new Father();
        Son s = new Son();
        // 子类覆盖对象 小于父类
        HashMap map = new HashMap<String, String>();
        TreeMap treeMap = new TreeMap();
        f.doSomeThing1(map);// 父类被执行
        s.doSomeThing1(map);// 子类被执行

        // 子类覆盖对象 大于父类
        f.doSomeThing2(map); // 父类被执行
        s.doSomeThing2(map); // 父类被执行,重载会
        s.doSomeThing2(treeMap); // 子类被执行
        System.out.println("汉字中文");
    }
}
