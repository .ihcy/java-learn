package org.example.design.pattern.factorymethod;

/**
 * @ClassName Product
 * @Description 产品
 **/
 abstract class Product {

    public void method1(){
        System.out.println("this is Product method1");
    }

    public abstract void method2();
}
