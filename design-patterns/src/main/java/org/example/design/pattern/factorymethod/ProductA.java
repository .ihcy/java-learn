package org.example.design.pattern.factorymethod;

/**
 * @ClassName ProductA
 * @Description
 **/
 class ProductA extends Product {

    @Override
    public void method2() {
        System.out.println("this is ProductA method2");
    }
}
