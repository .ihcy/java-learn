package org.example.design.pattern.proxypattern;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理类
 */
public class SubjectInvocationHandler implements InvocationHandler {

    private Subject target;
    public SubjectInvocationHandler(Subject target) {
        this.target = target;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println ("执行JDK的动态代理");
        Object object = method.invoke (target, args);
        System.out.println ("执行结果：" + object);
        return object;
    }
}
