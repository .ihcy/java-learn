package org.example.design.pattern.observerpattern.example.two;

public class Client {

    public static void main(String[] args) {
        Student student = new Student();
        Teacher teacher = new Teacher();
        BellEventSource bellEventSource = new BellEventSource();
        bellEventSource.addListener(student);
        bellEventSource.addListener(teacher);
        bellEventSource.ring(true);
        bellEventSource.ring(false);
    }
}
