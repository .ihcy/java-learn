package org.example.design.pattern.abstractfactory;

/**
 * @ClassName ProductB
 * @Description 抽象产品B
 **/
abstract class ProductB implements Product {

    @Override
    public void shareMethod() {
        System.out.println("产品B  的共有方法");
    }
}
