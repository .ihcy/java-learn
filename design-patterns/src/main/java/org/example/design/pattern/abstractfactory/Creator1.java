package org.example.design.pattern.abstractfactory;

/**
 * @ClassName Creator1
 * @Description 1 类产品的工厂
 **/
 class Creator1 extends Creator{
    @Override
    ProductA createProductA() {
        return new ProductA1 ();
    }

    @Override
    ProductB createProductB() {
        return new ProductB1 ();
    }
}
