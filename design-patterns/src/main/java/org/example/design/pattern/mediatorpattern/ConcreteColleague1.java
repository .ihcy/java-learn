package org.example.design.pattern.mediatorpattern;

public class ConcreteColleague1 extends Colleague {
    public ConcreteColleague1(Mediator mediator) {
        super (mediator);
    }

    /**
     * 自己处理的方法
     */
    public void selfMethod1() {
        System.out.println (this.getClass ().getSimpleName () + "selfMethod1");
    }

    /**
     * 需要外部处理的方法
     */
    public void denMethod1(){
        super.mediator.doSomething1 ();;
    }
}
