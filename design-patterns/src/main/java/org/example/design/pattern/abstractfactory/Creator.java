package org.example.design.pattern.abstractfactory;

/**
 * @ClassName Creator
 * @Description
 **/
abstract class Creator {
    abstract ProductA createProductA();
    abstract ProductB createProductB();
}
