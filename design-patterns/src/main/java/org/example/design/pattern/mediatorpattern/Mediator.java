package org.example.design.pattern.mediatorpattern;

/**
 * 抽象中介者，定义统一的接口，用于各同事角色中间的通信
 */
public abstract class Mediator {

    protected ConcreteColleague1 concreteColleague1;

    protected ConcreteColleague2 concreteColleague2;

    public Mediator(ConcreteColleague1 concreteColleague1, ConcreteColleague2 concreteColleague2) {
        this.concreteColleague1 = concreteColleague1;
        this.concreteColleague2 = concreteColleague2;
    }

    /**
     * 抽象自定义逻辑 1
     */
    public abstract void doSomething1();

    /**
     * 抽象自定义逻辑 2
     */
    public abstract void doSomething2();
}
