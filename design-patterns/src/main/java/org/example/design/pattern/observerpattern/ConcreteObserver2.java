package org.example.design.pattern.observerpattern;
/**
 * 具体观察者 2
 */
public class ConcreteObserver2 implements Observer {
    @Override
    public void response() {
        System.out.println(" 具体观察者2");
    }
}
