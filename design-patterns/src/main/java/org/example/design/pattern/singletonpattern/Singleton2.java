package org.example.design.pattern.singletonpattern;

/**
 * @ClassName Singleton2
 * @Description 饿汉式单例
 **/
public class Singleton2 {
    /**
     * volatile 关键字防止指令重排序
     */
    private volatile static Singleton2 singleton;
    private Singleton2() {
    }
    /**
     * 双重判断null 减少同步
     */
    public static Singleton2 getSingleton() {
        if (singleton == null) {
            synchronized (Singleton2.class) {
                if (singleton == null) {
                    singleton = new Singleton2();
                }
            }
        }
        return singleton;
    }
}
