package org.example.design.pattern.observerpattern;

/**
 * 具体观察者 1
 */
public class ConcreteObserver1 implements Observer {
    @Override
    public void response() {
        System.out.println(" 具体观察者1");
    }
}
