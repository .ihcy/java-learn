package org.example.design.pattern.observerpattern.example.two;

public class Student implements BellEventListener{
    @Override
    public void heardBell(RingEvent event) {
        if(event.isSound()){
            System.out.println("同学，上课了");
        }else {
            System.out.println("同学，下课了");
        }
    }
}
