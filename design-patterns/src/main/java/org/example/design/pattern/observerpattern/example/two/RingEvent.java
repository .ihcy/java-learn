package org.example.design.pattern.observerpattern.example.two;

import java.util.EventObject;

/**
 * 铃声事件 ： 用于封装事件源及一些与事件相关的参数
 */
public class RingEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    /**
     * 是否响铃 表示上课铃声,false表示下课铃声
     */
    private boolean sound;

    public RingEvent(Object source, boolean sound) {
        super(source);
        this.sound = sound;
    }

    public boolean isSound() {
        return sound;
    }

    public void setSound(boolean sound) {
        this.sound = sound;
    }
}
