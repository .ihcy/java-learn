package org.example.design.pattern.abstractfactory;

/**
 * @InterfaceName ProductA
 * @Description 抽象产品A
 **/
 abstract class ProductA implements Product{

    @Override
    public void shareMethod() {
        System.out.println("产品A的共有方法");
    }
}
