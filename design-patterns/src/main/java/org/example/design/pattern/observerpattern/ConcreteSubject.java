package org.example.design.pattern.observerpattern;

import java.util.List;

/**
 * 具体被观察者
 */
public class ConcreteSubject implements Subject {

    /**
     * 观察者集合
     */
    private List<Observer> observerList;

    public ConcreteSubject(List<Observer> observerList) {
        this.observerList = observerList;
    }

    @Override
    public void add(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void remove(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObserver() {
        System.out.println("具体 目标发生改变");
        System.out.println("***************");
        for (Observer observer : observerList) {
            observer.response();
        }
    }
}
