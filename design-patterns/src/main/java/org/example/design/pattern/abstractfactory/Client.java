package org.example.design.pattern.abstractfactory;

public class Client {

    public static void main(String[] args) {
        Creator creator1 = new Creator1();
        Creator creator2 = new Creator2();

        Product productA1 = creator1.createProductA();
        Product productB1 = creator1.createProductB();

        Product productA2 = creator2.createProductA();
        Product productB2 = creator2.createProductB();

        productA1.doSomething ();
        productA2.doSomething();
        productB1.doSomething();
        productB2.doSomething();
    }
}
