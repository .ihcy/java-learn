package org.example.design.pattern.factorymethod;

/**
 * @ClassName ProductA
 * @Description
 **/
 class ProductB extends Product {

    @Override
    public void method2() {
        System.out.println("this is ProductB method2");
    }
}
