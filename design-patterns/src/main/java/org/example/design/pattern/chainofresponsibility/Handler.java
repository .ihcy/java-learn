package org.example.design.pattern.chainofresponsibility;

public abstract class Handler {
   private Handler handler;

    public Handler getNext(){
       return this.handler;
   }

    public void setNext(Handler handler){
       this.handler = handler;
   }

   public abstract void handleRequest(String request);

}
