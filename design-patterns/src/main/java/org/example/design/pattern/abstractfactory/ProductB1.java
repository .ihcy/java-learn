package org.example.design.pattern.abstractfactory;

/**
 * @ClassName ProductA1
 * @Description
 **/
public class ProductB1 extends ProductB {
    @Override
    public void doSomething() {
        System.out.println("this is ProductB1");
    }
}
