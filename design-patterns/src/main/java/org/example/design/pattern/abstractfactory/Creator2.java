package org.example.design.pattern.abstractfactory;

/**
 * @ClassName Creator1
 * @Description 2 类产品的工厂
 **/
 class Creator2 extends Creator {
    @Override
    ProductA createProductA() {
        return new ProductA2 ();
    }

    @Override
    ProductB createProductB() {
        return new ProductB2 ();
    }
}
