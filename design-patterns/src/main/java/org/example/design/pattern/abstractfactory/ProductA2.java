package org.example.design.pattern.abstractfactory;

/**
 * @ClassName ProductA1
 * @Description
 **/
public class ProductA2 extends ProductA {
    @Override
    public void doSomething() {
        System.out.println("this is ProductA2");
    }
}
