package org.example.design.pattern.proxypattern;
/**
 * @Description 目标对象接口
 **/
public interface Subject {
    /**
     * 业务逻辑
     */
    void doSomething(String str1, int i);

    String getString(String str);
}
