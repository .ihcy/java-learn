package org.example.design.pattern.singletonpattern;

/**
 * @ClassName Singleton3
 * @Description 静态内部类实现单例
 * 
 * @Date 2019/11/26 19:19
 * @Version 1.0
 **/
public class Singleton3 {

    private Singleton3() {}

    public static Singleton3 getInstance(){
        return InnerSingleton3.singleton3;
    }

    private static class  InnerSingleton3{
        private static Singleton3 singleton3 = new Singleton3 ();
    }
}
