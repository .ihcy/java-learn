package org.example.design.pattern.observerpattern.example.two;

/**
 * 铃 监听
 */
public interface BellEventListener {

    /**
     * 事件处理 : 听到铃声
     * @param event
     */
    void heardBell(RingEvent event);
}
