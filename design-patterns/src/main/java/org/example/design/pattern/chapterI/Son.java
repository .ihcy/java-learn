package org.example.design.pattern.chapterI;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Son extends Father {
    public Collection doSomeThing1(HashMap map) {
        System.out.println("子类被执行");
        return map.values();
    }

    public Collection doSomeThing2(Map map) {
        System.out.println("子类被执行");
        return map.values();
    }
}
