package org.example.design.pattern.singletonpattern;

/**
 * @ClassName Singleton1
 * @Description 懒汉式单例
 **/
public class Singleton1 {
    private static Singleton1 singleton = new Singleton1 ();
    private Singleton1(){}
    /**
     * 懒汉式单例缺陷： 初始化加载单例对象，造成性能浪费，造成内存泄漏
     * @return
     */
    public static Singleton1 getSingleton(){
        return singleton;
    }
}
