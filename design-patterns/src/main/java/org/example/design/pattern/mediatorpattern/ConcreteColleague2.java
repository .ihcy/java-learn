package org.example.design.pattern.mediatorpattern;

public class ConcreteColleague2 extends Colleague {
    public ConcreteColleague2(Mediator mediator) {
        super (mediator);
    }

    /**
     * 自己处理的方法
     */
    public void selfMethod2() {
        System.out.println (this.getClass ().getSimpleName () + "： selfMethod2");
    }

    /**
     * 需要外部处理的方法
     */
    public void denMethod2(){

        super.mediator.doSomething2 ();
    }
}
