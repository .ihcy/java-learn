package org.example.design.pattern.abstractfactory;

/**
 * @ClassName ProductA1
 * @Description
 **/
public class ProductA1 extends ProductA {
    @Override
    public void doSomething() {
        System.out.println("this is ProductA1");
    }
}
