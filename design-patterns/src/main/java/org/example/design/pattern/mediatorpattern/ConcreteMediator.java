package org.example.design.pattern.mediatorpattern;

public class ConcreteMediator extends Mediator {

    public ConcreteMediator(ConcreteColleague1 concreteColleague1, ConcreteColleague2 concreteColleague2) {
        super (concreteColleague1, concreteColleague2);
    }

    @Override
    public void doSomething1() {
        concreteColleague1.selfMethod1 ();
    }

    @Override
    public void doSomething2() {
        concreteColleague2.selfMethod2 ();
    }
}
