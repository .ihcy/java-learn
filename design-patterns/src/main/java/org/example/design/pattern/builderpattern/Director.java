package org.example.design.pattern.builderpattern;

/**
 * @ClassName Director
 * @Description 导演类
 **/
public class Director {
    private Build build = new ConcreteBuilder ();

    public Product getProduct(){
        build.setPart();
        return build.buildProduct();
    }
}
