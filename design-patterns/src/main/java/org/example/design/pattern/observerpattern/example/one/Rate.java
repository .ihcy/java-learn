package org.example.design.pattern.observerpattern.example.one;

import java.util.ArrayList;
import java.util.List;

public abstract class Rate {
    public List<Company> companies;

    public Rate() {
        companies = new ArrayList<>();
    }

    public void add(Company company) {
        companies.add(company);
    }

    public void remove(Company company) {
        companies.remove(company);
    }

    /**
     * 汇率改变
     */
    public abstract void change(Integer num);
}
