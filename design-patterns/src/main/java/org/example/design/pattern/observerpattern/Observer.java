package org.example.design.pattern.observerpattern;

/**
 * 观察者
 * 收到信息后，对信息进行处理
 * 它包含了一个更新自己的抽象方法，当接到具体主题的更改通知时被调用
 */
public interface Observer {
    void response();
}
