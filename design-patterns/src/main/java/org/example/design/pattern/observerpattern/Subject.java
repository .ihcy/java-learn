package org.example.design.pattern.observerpattern;

/**
 * 被观察者
 * 职责：管理观察者，并通知观察者
 * 抽象目标：它提供了一个用于保存观察者对象的聚集类和增加、删除观察者对象的方法，以及通知所有观察者的抽象方法。
 */
public interface Subject {


    /**
     * 增加观察者
     */
    void add(Observer observer);

    /**
     * 删除观察者
     */
    void remove(Observer observer);

    /**
     * 通知观察者
     */
    abstract void notifyObserver();
}
