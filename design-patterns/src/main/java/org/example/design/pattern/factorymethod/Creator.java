package org.example.design.pattern.factorymethod;

/**
 * @ClassName Creator
 * @Description
 **/
  class Creator {
    public  <T extends Product> T createProduct(Class<T> clazz){
        Product product = null;
        try {
            product = (Product) Class.forName(clazz.getName()).newInstance();
        } catch (Exception e) {
            // 异常处理
            throw  new RuntimeException(e);
        }
        return (T) product;

    }

    public static void main(String[] args) {
        Creator creator = new Creator ();
        ProductA productA = creator.createProduct(ProductA.class);
        ProductB productB = creator.createProduct(ProductB.class);
        productA.method1();
        productA.method2();
        productB.method1();
        productB.method2();
    }
}
