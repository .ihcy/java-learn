package org.example.design.pattern.observerpattern.example.one;

public class RMBRate extends Rate {
    @Override
    public void change(Integer num) {
        for (Company company : companies) {
            company.response(num);
        }
    }
}
