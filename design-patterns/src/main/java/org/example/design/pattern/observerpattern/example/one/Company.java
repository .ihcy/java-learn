package org.example.design.pattern.observerpattern.example.one;

public interface Company {
    void response(int number);
}
