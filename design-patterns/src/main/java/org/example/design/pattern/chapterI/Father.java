package org.example.design.pattern.chapterI;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Father {
    public Collection doSomeThing1(Map map) {
        System.out.println("父类被执行");
        return map.values();
    }

    public Collection doSomeThing2(HashMap map) {
        System.out.println("父类被执行");
        return map.values();
    }
}
