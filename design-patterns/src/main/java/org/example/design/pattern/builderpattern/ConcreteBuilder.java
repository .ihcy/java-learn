package org.example.design.pattern.builderpattern;

/**
 * @ClassName ConcreteBuilder
 * @Description 具体建造者
 **/
public class ConcreteBuilder implements Build {
    @Override
    public void setPart() {
        // Product 操作
    }

    @Override
    public Product buildProduct() {
        return new ConcreteProduct();
    }
}
