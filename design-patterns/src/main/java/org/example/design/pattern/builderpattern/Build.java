package org.example.design.pattern.builderpattern;

/**
 * @ClassName Build
 * @Description 抽象建造者
 **/
interface Build {
    /**
     * 实现部分功能
     */
    void setPart();

    Product buildProduct();
}
