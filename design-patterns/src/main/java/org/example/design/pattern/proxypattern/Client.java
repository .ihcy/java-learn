package org.example.design.pattern.proxypattern;

import java.lang.reflect.Proxy;

public class Client {
    public static void main(String[] args) {
        Subject subject = new RealSubject ();
        // 获取代理类
        Subject proxy = (Subject) Proxy.newProxyInstance (
                Subject.class.getClassLoader (),
                subject.getClass ().getInterfaces (),
                new SubjectInvocationHandler (subject));
        proxy.doSomething ("proxy",1);
        proxy.getString ("test");
    }
}
