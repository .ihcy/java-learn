package org.example.design.pattern.observerpattern.example.two;

import java.util.ArrayList;
import java.util.List;

/**
 * 目标类 事件源 铃
 */
public class BellEventSource {

    private List<BellEventListener> listenerList;

    public BellEventSource() {
        this.listenerList = new ArrayList<>();
    }

    public void addListener(BellEventListener listener) {
        listenerList.add(listener);
    }

    public void removeListener(BellEventListener listener) {
        listenerList.remove(listener);
    }

    /**
     * 响铃处理
     */
    public void ring(boolean sound) {
        System.out.println((sound ? "上课铃" : "下课铃") + "声响起");
        RingEvent event = new RingEvent(this, sound);
        for (BellEventListener bellEventListener : listenerList) {
            bellEventListener.heardBell(event);
        }
    }

}
