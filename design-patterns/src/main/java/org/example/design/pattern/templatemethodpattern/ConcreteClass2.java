package org.example.design.pattern.templatemethodpattern;

public class ConcreteClass2 extends AbstractClass {

    /**
     * 基本方法
     */
    @Override
    protected void doSomething() {
        // 业务逻辑处理
    }

    /**
     * 基本方法
     */
    @Override
    protected void doAnything() {
        // 业务逻辑处理
    }
}
