package org.example.design.pattern.abstractfactory;

/**
 * @InterfaceName Product
 * @Description
 **/
interface Product {

    void shareMethod();

    void doSomething();
}
