# 1. [单例模式](src/main/java/org/example/design/pattern/singletonpattern)（Singleton Pattern）
> 确保某一个类只有一个实例，而且自行实例化并向整个系统提供这个实例。

*  [懒汉式单例](src/main/java/org/example/design/pattern/design/pattern/singletonpattern/Singleton1.java)
*  [饿汉式单例](src/main/java/org/example/design/pattern/design/pattern/singletonpattern/Singleton2.java)
*  [静态内部类](src/main/java/org/example/design/pattern/design/pattern/singletonpattern/Singleton3.java)

# 2. [工厂方法模式](src/main/java/org/example/design/pattern/factorymethod)（Factory Method Pattern）
> 定义一个用于创建对象的接口，让子类决定实例化哪一个类。工厂方法使一个类的实例化延迟到子类。

# 3. [抽象工厂模式](src/main/java/org/example/design/pattern/abstractfactory)（Abstract Factory Pattern）
> 为创建一组相关或者相互依赖的对象提供一个接口，而且无需指定它们的具体类

# 4. [建造者模式](src/main/java/org/example/design/pattern/builderpattern)（Builder Pattern）
>又叫做生成器模式。将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示。

* [产品](src/main/java/org/example/design/pattern/design/pattern/builderpattern/Product.java)
 通常是实现了模版方法模式，也就有模版方法和基本方法。

* [抽象构建者](src/main/java/org/example/design/pattern/design/pattern/builderpattern/Build.java)
规范产品的组件，一般是由子类实现。

* [具体构建者](src/main/java/org/example/design/pattern/design/pattern/builderpattern/ConcreteBuilder.java)
实现抽象类定义的所有方法，并且返回一个组建好的对象。

* [导演](src/main/java/org/example/design/pattern/design/pattern/builderpattern/Director.java)
负责安排已有模块的顺序，然后告诉Builder开始建造.
  
# 5. [模版方法模式](src/main/java/org/example/design/pattern/templatemethodpattern)(Template Method Pattern)
>定义一个操作中的算法的框架，而将一些步骤延迟到子类中。使得子类可以不改变一个算法的结构既可重定义该算法的某些特定步骤

* 基本方法：基本操作，是由子类实现的方法，并且在模版方法中被调用
* 模版方法： 可以有一个或者几个，一般是一个具体方法，也就是一个框架，实现对基本方法的调度，完成固定的逻辑

### 优点
* 封装不变部分，扩展可变部分
* 提取公共部分代码，便于维护
* 行为由父类控制，子类实现 

### 缺点
* 子类的执行的结果影响了父类的结果，复杂项目中，代码对新手可读性不高

# 6. [动态代理模式](src/main/java/org/example/design/pattern/proxypattern) (proxy pattern)
> 动态代理是在实现阶段不关心代理谁，而是在运行阶段才指定代理哪一个对象。

* Subject
[抽象主题角色](src/main/java/org/example/design/pattern/design/pattern/proxypattern/Subject.java)
* RealSubject
[具体主题角色](src/main/java/org/example/design/pattern/design/pattern/proxypattern/RealSubject.java)
* Proxy 代理主题角色
[JDK代理](src/main/java/org/example/design/pattern/design/pattern/proxypattern/SubjectInvocationHandler.java)
[JDK客户端](src/main/java/org/example/design/pattern/design/pattern/proxypattern/Client.java)
#### 动态代理产生代理对象的过程
1. 根据目标类接口去动态的编写一个java源文件（这个源文件对程序员是透明的）  
    ---所以说需要传递接口给jdk 动态代理的api编写的java源码文件需要一个增强器成员变量  
    ---所以说需要增强器对象给jdk 动态代理的api
2. 将java源文件编译成class字节码文件（这个编译过程和class字节码文件针对程序员也是透明的）
3. 将class字节码文件加载到jvm内存中（ClassLoader）---所以说需要传递Classloader给jdk 动态代理的api
4. 将加载到jvm中的Class信息，进行实例化（new 对象）

# 7 [中介者模式](src/main/java/org/example/design/pattern/mediatorpattern) (mediator pattern)
> 用一个中介对象封装一系列的对象交互，中介者使各对象不需要显示地相互作用，从而使其耦合松散，而且可以独立的改变他们之间的交互
 
* mediator [抽象中介者角色](src/main/java/org/example/design/pattern/design/pattern/mediatorpattern/Mediator.java)

* concrete mediator [具体中介者角色](src/main/java/org/example/design/pattern/design/pattern/mediatorpattern/ConcreteMediator.java)

* colleague [同事角色](src/main/java/org/example/design/pattern/design/pattern/mediatorpattern/Colleague.java)
* concrete colleague 1 [具体同事角色1](src/main/java/org/example/design/pattern/design/pattern/mediatorpattern/ConcreteColleague1.java)
* concrete colleague 2 [具体同事角色2](src/main/java/org/example/design/pattern/design/pattern/mediatorpattern/ConcreteColleague2.java)
#### 优点
* 减少了类的依赖，降低了类的耦合
#### 缺点
* 中介者变的膨胀的很大，而且逻辑复杂。

# 8 [责任链模式](src/main/java/org/example/design/pattern/chainofresponsibility)
> 为了避免请求发送者与多个请求处理者耦合在一起，于是将所有请求的处理者通过前一对象记住其下一个对象的引用而连成一条链；当有请求发生时，可将请求沿着这条链传递，直到有对象处理它为止。
> 本质是解耦请求和处理。
* 抽象处理者（Handler）角色：定义一个处理请求的接口，包含抽象处理方法和一个后继连接。
* 具体处理者（Concrete Handler）角色：实现抽象处理者的处理方法，判断能否处理本次请求，如果可以处理请求则处理，否则将该请求转给它的后继者。
* 客户类（Client）角色：创建处理链，并向链头的具体处理者对象提交请求，它不关心处理细节和请求的传递过程。

# 9 [观察者模式](src/main/java/org/example/design/pattern/observerpattern)
>在现实世界中，许多对象并不是独立存在的，其中一个对象的行为发生改变可能会导致一个或者多个其他对象的行为也发生改变。
> 例如，某种商品的物价上涨时会导致部分商家高兴，而消费者伤心；还有，当我们开车到交叉路口时，遇到红灯会停，遇到绿灯会行。

#### 定义
指多个对象间存在一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并自动更新。  
这种模式又称作为发布-订阅模式。

#### 模式结构
1. 抽象主题（Subject）: 
2. 具体主题（Concrete Subject）
3. 抽象观察者（Observer）
4. 具体观察者（Concrete Observer）
5. example.one 利用观察者模式设计一个程序，分析“人民币汇率”的升值或贬值对进口公司进口产品成本或出口公司的出口产品收入以及公司利润率的影响。
6. example.two 学校铃声的时间处理














