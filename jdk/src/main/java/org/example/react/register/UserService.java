package org.example.react.register;

public class UserService {

    private UserPublisher userPublisher;

    public boolean register(User user) {
        System.out.println(String.format("用户[%s]注册完成", user.getName()));
        return true;
    }

    public void sendEmail(User user) {
        System.out.println(String.format("向用户[%s] 发送邮件", user.getName()));
    }

    public void sendCoupon(User user) {
        System.out.println(String.format("向用户[%s] 发送新人用户体验卡券", user.getName()));
    }
}
