package org.example.react.register;

import java.util.concurrent.Flow;

/**
 * email 用户激活
 * @param <User>
 */
public class EmailSubscriber<User> implements Flow.Subscriber<User> {
    @Override
    public void onSubscribe(Flow.Subscription subscription) {

    }

    @Override
    public void onNext(User item) {

    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onComplete() {

    }
}
