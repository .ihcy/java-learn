package org.example.react.register;

import java.util.concurrent.Flow;

/**
 * 新人卷 体验卡
 * @param <User>
 */
public class CouponSubscriber<User> implements Flow.Subscriber<User> {



    @Override
    public void onSubscribe(Flow.Subscription subscription) {
       // 收到订阅请求
        System.out.println("发送新人体验卡");
    }

    @Override
    public void onNext(User item) {

    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onComplete() {

    }
}
