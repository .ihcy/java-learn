# JDK 相关基础

### Reactive Stream 响应式流

#### Reactive Stream 概念

Reactive Stream 是JDK 9 引入的一套标准，是一套基于发布/订阅模式的数据处理规范。 响应式流从2013年开始，作为提供非阻塞背压的异步流处理标准的倡议。  
旨在解决处理元素流的问题：如何将元素流从发布者传递到订阅者，而不需要发布者阻塞，或订阅者有无限制的缓冲区或丢弃。 反应式流（Reactive Stream） 规范诞生，定义了如下四个接口：

```text
Publisher<T> 接口定义了发布者的方法
Subscriber<T> 接口定义了订阅者的方法
Subscription 接口定义了连接发布者和订阅者的方法
Processor<T,R> 接口定义了处理器
```

#### 背压（back pressure）

如果生产者发出的信息比消费者能够处理消息最大量还要多，消费者可能会被迫一直在抓消息，耗费越来越多的资源，埋下潜在的崩溃风险。
为了防止这一点，需要有一种机制使消费者可以通知生产者，降低消息的生成速度。生产者可以采用多种策略来实现这一要求，这种机制称为背压。

#### JDK9 Reactive Stream 规范的实现

* JDK9中Reactive Stream的实现规范 通常被称为 Flow API 。 通过java.util.concurrent.Flow 和java.util.concurrent.SubmissionPublisher
  类来实现响应式流。
* 在JDK9里Reactive Stream的主要接口声明在Flow类里，Flow 类中定义了四个嵌套的静态接口， 用于建立流量控制的组件，发布者在其中生成一个或多个供订阅者使用的数据项：
    * Publisher：数据项发布者、生产者
    * Subscriber：数据项订阅者、消费者
    * Subscription：发布者与订阅者之间的关系纽带，订阅令牌
    * Processor：数据处理器

#### 接口源码分析

```java
/**
 * 用于建立流控制组件的相关接口和静态方法
 */
public final class Flow {

    private Flow() {
    }

    /**
     * 发布者
     */
    @FunctionalInterface
    public static interface Publisher<T> {
        /**
         * 给发布者添加订阅者
         */
        public void subscribe(Subscriber<? super T> subscriber);
    }

    /**
     * 订阅者 / 消息的接受者，此接口中方法按每个 subscription 严格顺序调用
     */
    public static interface Subscriber<T> {

        /**
         * 订阅者处理 相关信息
         * subscription 发布-订阅的链接
         */
        public void onSubscribe(Subscription subscription);

        /**
         * 订阅之后的下一个方法
         */
        public void onNext(T item);

        /**
         * 在发布者或订阅遇到不可恢复的错误时调用的方法，
         * 此后订阅不会调用其他订阅者方法。 
         * 如果此方法本身引发异常，则结果行为未定义。
         */
        public void onError(Throwable throwable);

        /**
         * 订阅终止
         */
        public void onComplete();
    }

    /**
     * 链接 Publisher 和 Subscriber
     */
    public static interface Subscription {
        /**
         * 将给定数量 n 的项目添加到此订阅者
         * @param n
         */
        public void request(long n);

        /**
         * 订阅者停止接受消息
         */
        public void cancel();
    }

    /**
     * 即充当订阅者又充当发布者
     * @param <T>
     * @param <R>
     */
    public static interface Processor<T, R> extends Subscriber<T>, Publisher<R> {
    }

    static final int DEFAULT_BUFFER_SIZE = 256;

    /**
     * 返回发布者或订阅者缓冲的默认值，可以在没有其他约束的情况下使用。
     */
    public static int defaultBufferSize() {
        return DEFAULT_BUFFER_SIZE;
    }
}
```

#### 博客
https://thepracticaldeveloper.com/reactive-programming-java-9-flow/