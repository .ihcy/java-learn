package org.example.tree;

public interface TreeFunction<T> {

    // 判断是否首节点
    boolean isRoot(T t);

    /**
     * 获取当前节点id
     */
    String getid(T t);

    /**
     * 获取父节点
     */
    String getPid(T t);
}
