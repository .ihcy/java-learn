package org.example.tree;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public static void main(String[] args) {
        List<Menu> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Menu menu = new Menu();
            menu.setId(String.valueOf(i));
            menu.setName("菜单-" + i);
            menu.setPid(i % 2 == 0 ? null : "");
            list.add(menu);

            for (int j = 0; j < 4 && i < 2; j++) {
                Menu temp = new Menu();
                temp.setId(menu.getId() + j);
                temp.setName(menu.getName() + j);
                temp.setPid(menu.getId());
                list.add(temp);
                for (int k = 0; k < 2 && j < 1; k++) {
                    Menu m = new Menu();
                    m.setId(temp.getId() + k);
                    m.setName(temp.getName() + k);
                    m.setPid(temp.getId());
                    list.add(m);
                }
            }
        }

        List<TreeNode<Menu>> treeNodes = TreeUtils.treeList(list, new TreeFunction<Menu>() {
            @Override
            public boolean isRoot(Menu menu) {
                return menu.getPid() == null || menu.getPid() == "";
            }

            @Override
            public String getid(Menu menu) {
                return menu.getId();
            }

            @Override
            public String getPid(Menu menu) {
                return menu.getPid();
            }
        });
        int i = 0;
    }
}
