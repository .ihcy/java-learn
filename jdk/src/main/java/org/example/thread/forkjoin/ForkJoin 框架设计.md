### Fork/Join 框架设计

#### Fork/Join 使用两个类来完成以上两件事情。

1. ForkJoinTask : 是用ForkJoin 框架，必须首先创建一个ForkJoin 任务。通常不需要直接继承ForkJoinTask 类，只需要继承它的子类。

   * RecursiveAction : 用于没有返回结果的任务

   * RecursiveTask：用于有返回结果的任务

2. ForkJoinPool : ForkJoinTask 需要通过ForkJoinPool 来执行。

#### Fork/Join 框架的实现原理
1. ForkJoinPool