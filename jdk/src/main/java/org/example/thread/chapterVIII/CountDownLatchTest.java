package org.example.thread.chapterVIII;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {

    static CountDownLatch latch = new CountDownLatch(2);

    public static void main(String[] args) throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(1);
                latch.countDown();
                System.out.println(2);
                latch.countDown();
            }
        }).start();
        // 导致当前线程等待直到锁存器倒计时到零
        latch.await();
        System.out.println(3);
    }
}
