package org.example.thread.chapterVIII;

/**
 * 解析一个Excel多个Sheet数据
 * 每个线程解析一个sheet数据，等到所有的sheet都解析完之后，程序需要提示解析完成。
 */
public class JoinCountDownLatchTest {

    public static void main(String[] args) throws InterruptedException {
        Thread parser1 = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        Thread parser2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("parser2 finish");
            }
        });
        parser1.start();
        parser2.start();
        // join 用于让当前执行线程等待join 执行结束。其实现原理是不停检查join线程是否存活。如果join线程存活则让当前线程永远等待
        parser1.join();
        parser2.join();
    }
}
