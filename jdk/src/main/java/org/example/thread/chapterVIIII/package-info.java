/**
 * 线程池好处
 * 1. 降低资源消耗。
 * 2. 提高响应速度。
 * 3. 提高线程的可管理性。
 */
package org.example.thread.chapterVIIII;

/**
 * 合理的配置线程池
 *
 */