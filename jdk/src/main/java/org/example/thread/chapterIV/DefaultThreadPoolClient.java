package org.example.thread.chapterIV;

import java.util.concurrent.TimeUnit;

public class DefaultThreadPoolClient {


    public static void main(String[] args) throws InterruptedException {
        DefaultThreadPool<Runner> pool = new DefaultThreadPool<>();
        for (int i = 0; i < 10; i++) {
            pool.execute(new Runner(i));
        }
        TimeUnit.SECONDS.sleep(10);
        pool.shutdown();

    }


    public static class Runner implements Runnable {
        private int i;

        public Runner(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + "is running " + i);
        }
    }

}
