package org.example.thread.chapterIV;

/**
 * 4.4.3 线程池
 * 客户端通过 excute() 方法将线程提交线程池执行，而客户端自身不用等待job的执行完成。
 * 这里的工作者线程代表着一个重复执行 的 线程，而每个客户端提交的 T 线程都将进入一个工作队列中等待工作者线程的处理。
 */
public interface ThreadPool<T extends Runnable> {
    /**
     * 执行线程
     */
    void execute(T t);

    /**
     * 关闭线程池
     */
    void shutdown();

    /**
     * 增加工作线程
     *
     * @param num
     */
    void addWorkers(int num);

    /**
     * 减少工作者线程
     */
    void removeWorker(int num);

    /**
     * 得到正在等待执行的任务数量
     */
    int getThreadSize();

}
