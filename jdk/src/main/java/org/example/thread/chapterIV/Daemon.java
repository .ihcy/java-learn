package org.example.thread.chapterIV;

/**
 * Daemon 是一种支持性线程，主要作为程序中后台调度以及支持性工作。
 * 当一个Java 虚拟机中不存在非 Daemon 线程的时候，Java虚拟机将推出
 * 可以  调用Thread.setDaemon(true); 将线程设置为 Daemon 线程
 */
public class Daemon {

    public static void main(String[] args) {
        Thread thread = new Thread(new DaemonRunner(),"DaemonRunner");
        thread.setDaemon(true);
        thread.start();
    }


    static class DaemonRunner implements Runnable{
        @Override
        public void run() {
            try {
                SleepUtils.second(10);
            } finally {
                // Daemon 线程中的finally 不一定被执行。不能依靠finally块中的内容来确保执行关闭或清理资源的逻辑
                System.out.println("DeamonThread finally run.");
            }
        }
    }
}
