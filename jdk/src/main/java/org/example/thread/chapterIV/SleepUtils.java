package org.example.thread.chapterIV;

import java.util.concurrent.TimeUnit;

public class SleepUtils {

    public final static void second(long second) {
        try {
            TimeUnit.SECONDS.sleep(second);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
