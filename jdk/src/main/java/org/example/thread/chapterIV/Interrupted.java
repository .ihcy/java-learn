package org.example.thread.chapterIV;

/**
 * 线程中断
 */
public class Interrupted {

    public static void main(String[] args) throws Exception {
        Thread sleepThread = new Thread(new SleepRunner(),"SleepRunner");
        sleepThread.setDaemon(true);

        Thread busyThread = new Thread(new BusyRunner(),"BusyRunner");
        busyThread.setDaemon(true);

        sleepThread.start();
        busyThread.start();
        // 休眠5秒，让 sleepThread 和 busyThread  充分运行
        Thread.sleep(5);

//        try {
//            sleepThread.interrupt();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        busyThread.interrupt();
        System.out.println("SleepThread interrupted is "+sleepThread.isInterrupted());
        System.out.println("BusyRunner interrupted is "+sleepThread.isInterrupted());
        // 防止 sleepThread 和 busyThread 立即退出
        SleepUtils.second(10);
    }

    static class SleepRunner implements Runnable {
        @Override
        public void run() {
            while (true) {
                SleepUtils.second(10);
            }
        }
    }

    static class BusyRunner implements Runnable {
        @Override
        public void run() {
            while (true) {

            }
        }
    }
}
